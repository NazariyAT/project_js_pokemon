window.onload = function () {

    initialCall();
    container();
    /* ------------ */
    header();
    /* headerLogo(); */
    headerNav();
    /* ------------ */
    pokedex();
    /*--------------*/
    /* footer();
    footerNav(); */
    
}



const getListPokemons = async () => {
    const result = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=150`);
    const resultToJson = await result.json();
    return resultToJson;
}

const recoverListPokemons = async () => {
    let listPokemon = await getListPokemons();
    return listPokemon;
}

const initialCall = async () => {
    let mappedList = await recoverListPokemons();
    mappedList.results.map((name) => {
        getPokemonDetail(name.url);
    })
}

const getPokemonDetail = async (url) => {
    const result = await fetch(url);
    const resultToJson = await result.json();
    printPokemonListDetail(resultToJson);
}

const printPokemonListDetail = async (pokemon) => {
    let $$pokedex = document.querySelector('.container__pokedex')
    $$pokedex.innerHTML += `<li>
    <p>${pokemon.name}</p>
    <img src=${pokemon.sprites.front_default} alt="${pokemon.name}"/>
    </li>`
}

